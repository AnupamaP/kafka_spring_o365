package com.cloudfx.api.common;

public enum KafkaMessageStatusEnumReader {

    SUCCESS("success"),
    ERROR("error");

    private String fieldName;

    private KafkaMessageStatusEnumReader(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public String toString() {
        return fieldName;
    }
}