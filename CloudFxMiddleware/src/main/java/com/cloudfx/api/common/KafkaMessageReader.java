package com.cloudfx.api.common;

import com.fasterxml.jackson.annotation.*;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data",
        "status",
        "message"
})
public class KafkaMessageReader {
	 @JsonProperty("data")
	    private Map<String, Object> data;

	    @JsonProperty("status")
	    private String status;

	    @JsonProperty("message")
	    private String message;

	    @JsonProperty("status")
	    public String getStatus() {
	        return status;
	    }

	    @JsonProperty("status")
	    public void setStatus(String status) {
	        this.status = status;
	    }

	    @JsonProperty("message")
	    public String getMessage() {
	        return message;
	    }

	    @JsonProperty("message")
	    public void setMessage(String message) {
	        this.message = message;
	    }

	    @JsonAnyGetter
	    public Map<String, Object> getData() {
	        return data;
	    }

	    @JsonAnySetter
	    public void setData(Map<String, Object> data) {
	        this.data = data;
	    }
}
