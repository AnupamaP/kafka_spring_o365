package com.cloudfx.api.producer;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.cloudfx.api.common.*;

@Service
public class KafkaProducerService {
 
	@Autowired
	ReplyingKafkaTemplate<String, KafkaMessageReader, KafkaMessageReader> kafkaTemplate;

	@Value("${jsa.kafka.topic}")
	String kafkaTopic;

	@Value("${kafka.getCustomer.topic}")
	String getCustomer;

	@PostMapping(path = "/getStatus", consumes = "application/json")
	public CompletableFuture<Void> getCustomer(@RequestBody Map<String, Object> data) {

		KafkaMessageReader msg = new KafkaMessageReader();
		msg.setMessage("Topic Name : " + kafkaTopic);
		msg.setStatus("Success");
		msg.setData(data);

		return kafkaTemplate.send(getCustomer, msg).completable()
				.thenAccept((org.springframework.kafka.support.SendResult<String, KafkaMessageReader> result) -> {
					System.out.println("in producer 40");
				}).exceptionally((Throwable ex) -> {
					System.out.println("Exception");
					return null;
				});

	}
}
