package com.cloudfx.api.util;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject; 
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

public class GenerateAccessToken {

	private static String grantType = "password";
	private static String resource = "https://api.partnercenter.microsoft.com";
	private static String clientId = "bd4774b6-2d94-4a2e-be49-0228f09866e1";
	private static String clientSecret = "lL9C0KC/R6otJsUb9T3sVUpOg5MXhksKrNPRPU1GIVU=";
	private static String loginHostname = "https://login.windows.net";
	private static String resellerDomainPrefix = "fnmtest.onmicrosoft.com";
	private static String username = "admin@fnmtest.onmicrosoft.com";
	private static String password="CFX@dm1n";

	public static String getAccessToken() throws JSONException {
		String responseString = "";
		final String hostname = loginHostname;
		final String url = hostname + "/" + resellerDomainPrefix + "/oauth2/token?api-version=1.0%20HTTP%2F1.1";
		Response response;

		System.out.println("grantType==" + grantType);
		System.out.println("resource===" + resource);
		System.out.println("clientId==" + clientId);
		System.out.println("clientSecret==" + clientSecret);
		System.out.println("loginHostname==" + loginHostname);
		System.out.println("resellerDomainPrefix==" + resellerDomainPrefix);
		System.out.println("username==" + username);
		System.out.println("password==" + password);

		MultipartBuilder builder = new MultipartBuilder();
		builder.addFormDataPart("grant_type", grantType);
		builder.addFormDataPart("resource", resource);
		builder.addFormDataPart("client_id", clientId);
		builder.addFormDataPart("client_secret", clientSecret);
		builder.addFormDataPart("username", username);
		builder.addFormDataPart("password", password);
		final RequestBody body = builder.type(MultipartBuilder.FORM).build();
		final Request request = new Request.Builder().url(url).post(body)
				.addHeader("content-type", "multipart/form-data").addHeader("cache-control", "no-cache").build();
		try {
			response = HTTPClientHelper.getOkHttpclient().newCall(request).execute();
			responseString = response.body().string();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(responseString);
		return new JSONObject(responseString).getString("access_token");
	}
}