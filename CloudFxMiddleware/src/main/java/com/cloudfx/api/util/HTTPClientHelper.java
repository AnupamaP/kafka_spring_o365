package com.cloudfx.api.util;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;

public class HTTPClientHelper {

	private static OkHttpClient okHttpclient;
	public static MediaType JSON = MediaType.parse("application/json;charset=utf-8");
	public static final MediaType XML = MediaType.parse("application/xml;charset=utf-8");

	public static OkHttpClient getOkHttpclient() {

		if (okHttpclient == null) {
			// Proxy proxyTest = new Proxy(Proxy.Type.HTTP,new
			// InetSocketAddress("proxy.etisalat.corp.ae", 8080));
			okHttpclient = new OkHttpClient();
			okHttpclient.setReadTimeout((long) 120, TimeUnit.SECONDS);
			// okHttpclient.setProxy(proxyTest);
		}

		return okHttpclient;
	}

	public static void closeHttpClient(Response response, String action) {
		JSONObject metaJson = new JSONObject();
		if (response != null) {
			try {
				response.body().close();
			} catch (IOException exception) {
				exception.printStackTrace();
			}
		}
	}
}
