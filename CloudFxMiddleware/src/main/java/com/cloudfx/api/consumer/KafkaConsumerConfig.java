//package com.cloudfx.api.consumer;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import org.apache.kafka.clients.consumer.ConsumerConfig;
//import org.apache.kafka.common.serialization.StringDeserializer;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.kafka.annotation.EnableKafka;
//import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
//import org.springframework.kafka.core.ConsumerFactory;
//import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
//import org.springframework.kafka.core.ProducerFactory;
//import org.springframework.kafka.listener.KafkaMessageListenerContainer;
//import org.springframework.kafka.listener.config.ContainerProperties;
//import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
//
//import com.cloudfx.api.common.KafkaMessageReader;
//
//@Configuration
//@EnableKafka
//public class KafkaConsumerConfig {
//	@Value("${jsa.kafka.bootstrap-servers}")
//	private String bootstrapServer;
//
//	@Value("${jsa.kafka.consumer.group-id}")
//	private String groupId;
//
//	  @Value("${spring.kafka.request.reply.topic}")
//	    private String requestReplyTopic;
//	@Bean
//	public ConsumerFactory<String, String> consumerFactory() {
//		Map<String, Object> props = new HashMap<>();
//		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
//		props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
//		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
//		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
//		return new DefaultKafkaConsumerFactory<>(props);
//	}
//
//	@Bean
//	public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory() {
//		ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
//		factory.setConsumerFactory(consumerFactory());
//		return factory;
//	}
//	@Bean
//	public ReplyingKafkaTemplate<String, String, String> replyKafkaTemplate(
//			ProducerFactory<String, String> pf,
//			KafkaMessageListenerContainer<String, String> container) {
//		return new ReplyingKafkaTemplate<>(pf, container);
//	}
//
//	@Bean
//	 public KafkaMessageListenerContainer<String, String>
//	 replyContainer(ConsumerFactory<String, String> cf) {
//	 ContainerProperties containerProperties = new
//	 ContainerProperties(requestReplyTopic);
//	 return new KafkaMessageListenerContainer<>(cf, containerProperties);
//	 }
//}
