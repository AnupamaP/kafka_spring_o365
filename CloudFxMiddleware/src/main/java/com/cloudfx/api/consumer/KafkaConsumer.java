package com.cloudfx.api.consumer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.cloudfx.api.common.KafkaMessageReader;
import com.cloudfx.api.util.GenerateAccessToken;
import com.cloudfx.api.util.GetClientHelper;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.Response;

@Component
public class KafkaConsumer {
 

	@KafkaListener(topics = "${kafka.getCustomer.topic}")
	public void getCustomer(@Payload KafkaMessageReader kafkaMessage, @Headers MessageHeaders headers) throws IOException, JSONException {
		System.out.println("------kafka.getCustomer.topic--------");
	
 
		String tenantId = kafkaMessage.getData().get("tenantId").toString();
		GenerateAccessToken g = new GenerateAccessToken();

		String token = g.getAccessToken();
		System.out.println("token =====" + token);
		String url = "/v1/customers/" + tenantId;
		Response response = null;
		try {
			response = GetClientHelper.execute("get_customer", url, 0, token);
			System.out.println(response.body().string());
 		} catch (IOException e) {
 			e.printStackTrace();
			System.out.println(response.body().string());
 
		}
	}
}
