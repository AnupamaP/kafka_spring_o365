package com.cloudfx.api.controller;
 
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
 import org.springframework.web.bind.annotation.RestController;

import com.cloudfx.api.common.KafkaMessageReader;
 import com.cloudfx.api.producer.KafkaProducerService; 
  
@RestController
@RequestMapping("/api/o365")
public class O365Controller {

	@Autowired
	private KafkaProducerService kafkaProducer;
 

	@PostMapping(path = "/getCustomer", consumes = "application/json")
	public void getCustomer(@RequestBody Map<String, Object> payload) {
		kafkaProducer.getCustomer(payload);
 	}
	
	

 

}