package com.infiverve.cloudselect.mw.o365.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import okio.Buffer;

public class HttpsHelper {

	public static MediaType JSON = MediaType.parse("application/json;charset=utf-8");
	public static MediaType XML = MediaType.parse("text/xml; charset=utf-8");
	public static MediaType SOAP = MediaType.parse("text/xml; charset=utf-8");
	static int httpStatusCode;

	public static Response execute(String action, String url, String body, int identifier, String token)
			throws IOException {

		System.out.println("body ======" + body);
		Request request = null;
		String hostname = null;
		RequestBody requestBody = RequestBody.create(JSON, body);

		switch (identifier) {
		case 0:
			hostname = "https://api.partnercenter.microsoft.com";

			url = hostname + url;
			System.out.println(url);
			request = new Request.Builder().addHeader("Accept", "application/json")
					.addHeader("MS-RequestId", UUID.randomUUID().toString())
					.addHeader("MS-CorrelationId", UUID.randomUUID().toString())
					.addHeader("Content-Type", "application/json").addHeader("Authorization", "Bearer " + token)
					.url(url).post(requestBody).build();

			break;

		case 1:

			hostname = "https://api.partnercenter.microsoft.com";

			url = hostname + url;
			System.out.println(url);
			request = new Request.Builder().addHeader("Accept", "application/json")
					.addHeader("MS-RequestId", UUID.randomUUID().toString())
					.addHeader("MS-CorrelationId", UUID.randomUUID().toString())
					.addHeader("Content-Type", "application/json").addHeader("Authorization", "Bearer " + token)
					.url(url).delete(requestBody).build();

			break;

		case 2:

			hostname = "https://api.partnercenter.microsoft.com";

			url = hostname + url;
			System.out.println(url);
			request = new Request.Builder().addHeader("Accept", "application/json")
					.addHeader("MS-RequestId", UUID.randomUUID().toString())
					.addHeader("MS-CorrelationId", UUID.randomUUID().toString())
					.addHeader("Content-Type", "application/json").addHeader("Authorization", "Bearer " + token)
					.url(url).get().build();

			break;
		case 3:
			hostname = "https://api.partnercenter.microsoft.com";
			url = hostname + url;
			System.out.println(url);
			System.out.println(requestBodyToString(requestBody));
			request = new Request.Builder().addHeader("Accept", "application/json")
					.addHeader("MS-RequestId", UUID.randomUUID().toString())
					.addHeader("MS-CorrelationId", UUID.randomUUID().toString()).addHeader("Content-Type", "application/json")
					.addHeader("Authorization", "Bearer " + token).url(url)
					.patch(requestBody).build();

			break;
		default:
			break;
		}

		final Response response = HTTPClientHelper.getOkHttpclient().newCall(request).execute();

		return response;

	}

	public static String requestBodyToString(RequestBody requestBody) {
		try {
			Buffer buffer = new Buffer();
			requestBody.writeTo(buffer);
			return buffer.readUtf8();
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
}
