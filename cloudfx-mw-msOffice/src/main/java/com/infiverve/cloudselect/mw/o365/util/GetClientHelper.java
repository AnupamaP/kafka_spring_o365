package com.infiverve.cloudselect.mw.o365.util;

import java.io.IOException;
import java.util.UUID; 
 
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

public class GetClientHelper {
 
	  private static final OkHttpClient client = HTTPClientHelper.getOkHttpclient();

	  public static Response execute(String loggerAction, String url, int identifier, String token) throws IOException {

 
	    Request request = null;
	    String hostname = null;
 
	    switch (identifier) {
	    case 0:

	      hostname = "https://api.partnercenter.microsoft.com";

	      url = hostname + url;
	      System.out.println("URL--------------------->"+url);
	      request = new Request.Builder().addHeader("Accept", "application/json")
	          .addHeader("Authorization", "Bearer " + token)
	          .addHeader("Connection", "Keep-Alive") 
	          .addHeader("MS-RequestId",UUID.randomUUID().toString())
	          .addHeader("MS-CorrelationId", UUID.randomUUID().toString())
	          .url(url).get().build(); 
 	      break;

	    case 1:

	      String sharedKey = "E3923B2E5D2F385D7AAD33D83E38B";
	      hostname = "localhost";
	      String port = "9012";
	      
	      url = "http://" + hostname + ":" + port + url;
 
	      request = new Request.Builder().url(url).get().build();

	      break;
	    case 2:

	      hostname ="localhost";

	      url = hostname + url;

	      request = new Request.Builder().addHeader("Accept", "application/json")
	          .addHeader("Authorization", "Bearer " + token)
	          .addHeader("Content-Type", "application/json")
	          .url(url).get().build(); //.addHeader("Connection", "Keep-Alive") .addHeader("MS-RequestId", UTIL.getUUID()).addHeader("MS-CorrelationId", UTIL.getUUID())

 
	      break;
	      
	    case 3:
	      hostname = "localhost";
	      url = hostname + url;

	      request = new Request.Builder().addHeader("Accept", "application/json")
	          .addHeader("Authorization", "Bearer " + token)
	          .addHeader("Connection", "Keep-Alive") 
	          .addHeader("MS-RequestId", UUID.randomUUID().toString())
	          .addHeader("MS-CorrelationId", UUID.randomUUID().toString())
	          .url(url).head().build(); 
	      break;
	    default:
	      break;
	    }
 	    final Response response = HTTPClientHelper.getOkHttpclient().newCall(request).execute();
 	    return response;
 	  }
 
}
