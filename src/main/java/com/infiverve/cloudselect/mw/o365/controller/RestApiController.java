package com.infiverve.cloudselect.mw.o365.controller;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.IIOException;
import javax.validation.Payload;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.env.SystemEnvironmentPropertySource;
import org.springframework.http.StreamingHttpOutputMessage.Body;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.couchbase.client.java.document.json.JsonArray;
import com.couchbase.client.java.document.json.JsonObject;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.infiverve.cloudselect.mw.o365.util.GenerateAccessToken;
import com.infiverve.cloudselect.mw.o365.util.GetClientHelper;
import com.infiverve.cloudselect.mw.o365.util.HttpsHelper;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import rx.internal.util.SynchronizedSubscription;

@RestController
@RequestMapping("/api")
public class RestApiController {

	@Value("${conf.resource}")
	private String resource;

	@Value("${conf.grantType}")
	private String grantType;

	@Value("${conf.clientId}")
	private String clientId;

	@Value("${conf.clientSecret}")
	private String clientSecret;

	@Value("${conf.loginHostname}")
	private String loginHostname;

	@Value("${conf.resellerDomainPrefix}")
	private String resellerDomainPrefix;

	@Value("${conf.username}")
	private String username;

	@Value("${conf.password}")
	private String password;

	public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);
	private static OkHttpClient okHttpclient;

	@RequestMapping(value = "/getCustomer", method = RequestMethod.POST)
	public Response getAvailableLicences(@RequestBody Map<Object, Object> payload) {

		System.out.println(payload);
		String result = "";
		String tenantId = payload.get("tenantId").toString();
		String token = GenerateAccessToken.getAccessToken(grantType, resource, clientId, clientSecret, loginHostname,
				resellerDomainPrefix, username, password);
		String url = "/v1/customers/" + tenantId + "/subscribedskus";
		Response response = null;
		try {
			response = GetClientHelper.execute("get_all_customer", url, 0, token);
			result = response.body().string();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}

	@RequestMapping(value = "/createcustomer", method = RequestMethod.POST)
	public String createCustomer(@RequestBody Map<Object, Object> payload) throws IOException {
		JSONObject newCustomerData = new JSONObject();
		JSONObject billingProfile = new JSONObject();
		JSONObject companyProfile = new JSONObject();
		String token = GenerateAccessToken.getAccessToken(grantType, resource, clientId, clientSecret, loginHostname,
				resellerDomainPrefix, username, password);
		Response response = null;
		try {
			String domain = payload.get("domain").toString();
			String firstName = payload.get("firstName").toString();
			String lastName = payload.get("lastName").toString();
			String email = payload.get("email").toString();
			String companyName = payload.get("companyName").toString();
			String culture = payload.get("culture").toString();
			String language = payload.get("language").toString();
			String country = payload.get("country").toString();
			String city = payload.get("city").toString();
			String state = payload.get("state").toString();
			String addrLine1 = payload.get("addrLine1").toString();
			String postalCode = payload.get("postalCode").toString();

			companyProfile.put("Domain", domain);

			billingProfile.put("FirstName", firstName).put("LastName", lastName).put("Email", email)
			.put("CompanyName", companyName).put("Culture", culture).put("Language", language);

			final JSONObject defaultAddress = new JSONObject().put("Country", country).put("City", city)
					.put("State", state).put("AddressLine1", addrLine1).put("PostalCode", postalCode)
					.put("LastName", firstName).put("FirstName", lastName);

			billingProfile.putOpt("DefaultAddress", defaultAddress);
			newCustomerData.putOpt("CompanyProfile", companyProfile).putOpt("BillingProfile", billingProfile);

			response = HttpsHelper.execute("create_o365_customer", "/v1/customers", newCustomerData.toString(), 0,
					token);
			System.out.println(response.body().string());
			return response.body().string();

		} catch (Exception e) {
			e.printStackTrace();
			return response.body().string();
		}
	}

	@RequestMapping(value = "/deleteCustomer", method = RequestMethod.POST)
	public String deleteCustomerUserAccount(@RequestBody Map<Object, Object> payload) throws Exception {
		JSONObject newCustomerData = new JSONObject();
		String token = GenerateAccessToken.getAccessToken(grantType, resource, clientId, clientSecret, loginHostname,
				resellerDomainPrefix, username, password);
		Response response = null;
		try {
			System.out.println(token);
			String tenantId = payload.get("tenantId").toString();
			response = HttpsHelper.execute("delete_customer", "/v1/customers/" + tenantId, newCustomerData.toString(),
					1, token);
			return response.body().string();

		} catch (Exception e) {
			return response.body().string();
		}
	}
	@RequestMapping(value = "/createOrder", method = RequestMethod.POST)
	public String createOrder(@RequestBody Map<Object, Object> payload) throws IOException {
		Response response = null;
		try {
			JSONObject requestJson = new JSONObject();
			String tenantId = payload.get("tenantId").toString();

			System.out.println(payload.get("LineItems"));
			// System.out.println(lineItemArr);

			// for (int i = 0; i < lineItemArr.size(); i++) {
			// System.out.println(lineItemArr.get(i));
			//
			// JSONArray tempProvisioningArray = new JSONArray();
			// JSONObject jsonobject = lineItemArr.getJSONObject(i);
			// int lineItemNumber = jsonobject.getInt("LineItemNumber");
			// String offerId = jsonobject.getString("OfferId");
			// String friendlyName = jsonobject.getString("FriendlyName");
			// String quantity = jsonobject.getString("Quantity");
			//
			// System.out.println("LineItemNumber" + lineItemNumber);
			// System.out.println("OfferId" + offerId);
			// System.out.println("friendlyName" + friendlyName);
			// System.out.println("friendlyName" + friendlyName);
			// System.out.println("quantity" + quantity);
			// JSONArray provisioningArray =
			// lineItemArr.getJSONObject(i).getJSONArray("ProvisioningContext");
			// for (int j = 0; j < provisioningArray.length(); j++) {
			// String scope = provisioningArray.getJSONObject(i).getString("scope");
			// String duration = provisioningArray.getJSONObject(i).getString("duration");
			// System.out.println("scope" + scope);
			// System.out.println("duration" + duration);
			// }

			// }
			String token = GenerateAccessToken.getAccessToken(grantType, resource, clientId, clientSecret,
					loginHostname, resellerDomainPrefix, username, password);
			response = HttpsHelper.execute("create_o365_order", "/v1/customers/" + tenantId + "/orders",
					payload.toString(), 1, token);
			return response.body().string();
		} catch (Exception e) {
			e.printStackTrace();
			return response.body().string();
		}
	}

	@RequestMapping(value = "/getAllSubscriptions", method = RequestMethod.POST)
	public String getAllSubscriptions(@RequestBody Map<Object, Object> payload) throws IOException {
		String tenantId = payload.get("tenantId").toString();
		Response response = null;
		try {
			String token = GenerateAccessToken.getAccessToken(grantType, resource, clientId, clientSecret,
					loginHostname, resellerDomainPrefix, username, password);
			response = HttpsHelper.execute("get_all_subscriptions", "/v1/customers/" + tenantId + "/subscriptions ",
					payload.toString(), 2, token);
			return response.body().string();
		} catch (Exception e) {
			e.printStackTrace();
			return response.body().string();
		}

		/*
		 * Get licenses assigned to a user
		 * https://docs.microsoft.com/en-us/partner-center/develop/check-which-licenses-
		 * are-assigned-to-a-user
		 */

	}

	@RequestMapping(value = "/suspendedsubscription", method = RequestMethod.POST)
	public String suspendSubscription(@RequestBody Map<Object, Object> payload) throws IOException {
		String tenantId = payload.get("Id").toString();
		String subscriptionId = payload.get("OrderId").toString();
		payload.put("Status", "suspended");

		for (Entry<Object, Object> entry : payload.entrySet()) {
			System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
		}
		Response response = null;
		try {
			String token = GenerateAccessToken.getAccessToken(grantType, resource, clientId, clientSecret,
					loginHostname, resellerDomainPrefix, username, password);
			response = HttpsHelper.execute("suspend_subscription",
					"/v1/customers/" + tenantId + "/subscriptions/" + subscriptionId, payload.toString(), 3, token);
			return response.body().string();
		} catch (Exception e) {
			e.printStackTrace();
			return response.body().string();
		}
	}

	@RequestMapping(value = "/getSubscriptionBydId", method = RequestMethod.POST)
	public String getSubscriptionBydId(@RequestBody Map<Object, Object> payload) throws IOException {
		String tenantId = payload.get("tenantId").toString();
		String subscriptionId = payload.get("subscriptionId").toString();
		Response response = null;
		try {
			String token = GenerateAccessToken.getAccessToken(grantType, resource, clientId, clientSecret,
					loginHostname, resellerDomainPrefix, username, password);
			response = HttpsHelper.execute("suspend_subscription",
					"/v1/customers/" + tenantId + "/subscriptions/" + subscriptionId, payload.toString(), 2, token);
			return response.body().string();
		} catch (Exception e) {
			e.printStackTrace();
			return response.body().string();
		}
	}
	@RequestMapping(value = "/getListAvailableLicenses", method = RequestMethod.POST)
	public String getListAvailableLicenses(@RequestBody Map<Object, Object> payload) throws IOException {
		String tenantId = payload.get("tenantId").toString();
		Response response = null;
		try {
			String token = GenerateAccessToken.getAccessToken(grantType, resource, clientId, clientSecret,
					loginHostname, resellerDomainPrefix, username, password);
			response = HttpsHelper.execute("get_list_available_licenses",
					"/v1/customers/" + tenantId + "/subscribedskus", payload.toString(), 2, token);
			return response.body().string();
		} catch (Exception e) {
			e.printStackTrace();
			return response.body().string();
		}
	}
	// Get licenses assigned to a user
	@RequestMapping(value = "/getLicensesUsers", method = RequestMethod.POST)
	public String getLicensesAssignedToUser(@RequestBody Map<Object, Object> payload) throws IOException {
		String tenantId = payload.get("tenantId").toString();
		String user_id = payload.get("user_id").toString();
		Response response = null;
		JSONObject jsonObject = new JSONObject();
		try {
			String token = GenerateAccessToken.getAccessToken(grantType, resource, clientId, clientSecret,
					loginHostname, resellerDomainPrefix, username, password);
			response = HttpsHelper.execute("get_lincenses_user",
					"/v1/customers/" + tenantId + "/users/" + user_id + "/licenses", payload.toString(), 2, token);
			return response.body().string();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response.body().string();
	}
	// Assign licenses To users
	@RequestMapping(value = "/assignLicensesToUser", method = RequestMethod.POST)
	public String assignLicensesToUsers(@RequestBody Map<Object, Object> payload) throws Exception {
		JSONObject newCustomerData = new JSONObject();
		JSONObject billingProfile = new JSONObject();
		JSONObject companyProfile = new JSONObject();
		String token = GenerateAccessToken.getAccessToken(grantType, resource, clientId, clientSecret, loginHostname,
				resellerDomainPrefix, username, password);
		Response response = null;
		try {
			String customer_id = payload.get("customer_id").toString();
			String user_id = payload.get("user_id").toString();
			String skuId = payload.get("SkuId").toString();
			companyProfile.put("customer_id", customer_id);
			JSONObject gson = new JSONObject();
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			jsonObject.put("skuId", skuId);
			jsonArray.put(jsonObject);
			JSONObject jsonObjectNew = new JSONObject();
			jsonObjectNew.put("licensesToAssign", jsonArray);
			JSONObject jsonObject2 = new JSONObject();
			jsonObject2.put("objectType", "LicenseUpdate");
			jsonObjectNew.put("attributes", jsonObject2);
			System.out.println("++++++++++++++++++ " + jsonObjectNew);
			response = HttpsHelper.execute("get_lincenses_user",
					"/v1/customers/" + customer_id + "/users/" + user_id + "/licenseupdates", jsonObjectNew.toString(),
					0, token);
			return response.body().string();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			return response.body().string();
		}
	}
	// Get a list of add-ons for a subscription
	@RequestMapping(value = "/getListOfAddOnsForSubscription", method = RequestMethod.POST)
	public String getListOfSubscription(@RequestBody Map<Object, Object> payload) throws IOException {
		String tenantId = payload.get("tenantId").toString();
		String subscriptionId = payload.get("subscriptionId").toString();
		Response response = null;
		JSONObject jsonObject = new JSONObject();
		try {
			String token = GenerateAccessToken.getAccessToken(grantType, resource, clientId, clientSecret,
					loginHostname, resellerDomainPrefix, username, password);
			response = HttpsHelper.execute("get_lincenses_user",
					"/v1/customers/" + tenantId + "/subscriptions/" + subscriptionId + "/addons ", payload.toString(),
					2, token);
			System.out.println("Response " + response);
			return response.body().string();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response.body().string();
	}
	// Reactivate a suspended subscription
	
	@RequestMapping(value = "/reactivateSuspendedSubscription", method = RequestMethod.PATCH)
	public String reactiveSuspendedSubscription(@RequestBody Map<Object, Object> payload) throws IOException {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("Id", payload.get("Id")).put("FriendlyName", payload.get("FriendlyName"))
		.put("Attributes", new JSONObject().put("Etag", "Etag").put("ObjectType", "Subscription"))
		.put("Quantity", payload.get("Quantity")).put("UnitType", payload.get("UnitType"))
		.put("ParentSubscriptionId", payload.get("ParentSubscriptionId"))
		.put("CreationDate", payload.get("CreationDate"))
		.put("EffectiveStartDate", payload.get("EffectiveStartDate"))
		.put("CommitmentEndDate", payload.get("CommitmentEndDate")).put("Status", payload.get("Status"))
		.put("AutoRenewEnabled", payload.get("AutoRenewEnabled")).put("BillingType", payload.get("BillingType"))
		.put("PartnerId", payload.get("PartnerId")).put("ContractType", payload.get("ContractType"))
		.put("OrderId", payload.get("OrderId"));
		System.out.println("JsonObject <============> " + jsonObject);
		String customer_tenant_id = payload.get("customer_tenant_id").toString();
		String id_for_subscription = payload.get("id_for_subscription").toString();
		String token = GenerateAccessToken.getAccessToken(grantType, resource, clientId, clientSecret, loginHostname,
				resellerDomainPrefix, username, password);
		Response response = null;
		try {
			System.out.println("Response" + response);
			response = HttpsHelper.execute("Reactivate_suspended_subscription",
					"/v1/customers/" + customer_tenant_id + "/subscriptions/" + id_for_subscription + "",
					jsonObject.toString(), 3, token);
			return response.body().string();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response.body().string();
	}
	// Change the quantity of a subscription
	@RequestMapping(value = "/changeTheQuantityOfSubscription", method = RequestMethod.PATCH)
	public String changQuantityOfSubscription(@RequestBody Map<Object, Object> payload) throws IOException {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("Id", payload.get("Id")).put("FriendlyName", payload.get("FriendlyName"))
		.put("Attributes", new JSONObject().put("Etag", "Etag").put("ObjectType", "Subscription"))
		.put("Quantity", payload.get("Quantity")).put("UnitType", payload.get("UnitType"))
		.put("ParentSubscriptionId", payload.get("ParentSubscriptionId"))
		.put("CreationDate", payload.get("CreationDate"))
		.put("EffectiveStartDate", payload.get("EffectiveStartDate"))
		.put("CommitmentEndDate", payload.get("CommitmentEndDate")).put("Status", payload.get("Status"))
		.put("AutoRenewEnabled", payload.get("AutoRenewEnabled")).put("BillingType", payload.get("BillingType"))
		.put("PartnerId", payload.get("PartnerId")).put("ContractType", payload.get("ContractType"))
		.put("OrderId", payload.get("OrderId"));
		System.out.println("JsonObject <============> " + jsonObject);
		String customer_tenant_id = payload.get("customer_tenant_id").toString();
		String id_for_subscription = payload.get("id_for_subscription").toString();
		String token = GenerateAccessToken.getAccessToken(grantType, resource, clientId, clientSecret, loginHostname,
				resellerDomainPrefix, username, password);
		Response response = null;
		try {
			System.out.println("Response" + response);
			response = HttpsHelper.execute("Reactivate_suspended_subscription",
					"/v1/customers/" + customer_tenant_id + "/subscriptions/" + id_for_subscription + "",
					jsonObject.toString(), 3, token);
			return response.body().string();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response.body().string();
	}
	// Update user accounts for a customer

	@RequestMapping(value = "/updateUserAccountForCustomer", method = RequestMethod.PATCH)
	public String updateUserAccountForCustomer(@RequestBody Map<Object, Object> payload) throws IOException {
		JSONObject jsonobject = new JSONObject();
		jsonobject.put("usageLocation", payload.get("usageLocation")).put("JSONObject",
				new JSONObject().put("objectType", "CustomerUser"));
		System.out.println("===========Jsonobject==========" + jsonobject);
		String customer_tenant_id = payload.get("customer_tenant_id").toString();
		String user_id = payload.get("user_id").toString();
		String token = GenerateAccessToken.getAccessToken(grantType, resource, clientId, clientSecret, loginHostname,
				resellerDomainPrefix, username, password);
		Response response = null;
		try 
		{
			response = HttpsHelper.execute("Update_user _ccounts_for_ustomer","/v1/customers/" + customer_tenant_id + "/users/"+user_id+" ", jsonobject.toString(), 3, token);
			return response.body().string();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response.body().string();

	}
}
