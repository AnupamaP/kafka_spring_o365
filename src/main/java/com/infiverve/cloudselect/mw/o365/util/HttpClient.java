package com.infiverve.cloudselect.mw.o365.util;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class HttpClient {

	// private String server = "http://localhost:3000";
	private RestTemplate rest;
	private HttpHeaders headers;
	private HttpStatus status;

	public HttpClient() {
		this.rest = new RestTemplate();
		this.headers = new HttpHeaders();
		//headers.add("Content-Type", "application/json");
		//headers.add("Accept", "*/*");
		//headers.add("Authorization", "Bearer YTyjNvNL2A7vA59QK7ExnnpjjqTgRsL5");
	}

	public String get(String url,HttpHeaders headers1) {

		HttpEntity<String> requestEntity = new HttpEntity<String>("", headers1);
		ResponseEntity<String> responseEntity = rest.exchange(url, HttpMethod.GET, requestEntity, String.class);
		this.setStatus(responseEntity.getStatusCode());
		return responseEntity.getBody();
	}

	public String post(String url, String json, HttpHeaders headers1) {
		HttpEntity<String> requestEntity = new HttpEntity<String>(json, headers1);
		ResponseEntity<String> responseEntity = rest.exchange(url, HttpMethod.POST, requestEntity, String.class);
		this.setStatus(responseEntity.getStatusCode());
		return responseEntity.getBody();
	}

	public void put(String url, String json) {
		HttpEntity<String> requestEntity = new HttpEntity<String>(json, headers);
		ResponseEntity<String> responseEntity = rest.exchange(url, HttpMethod.PUT, requestEntity, String.class);
		this.setStatus(responseEntity.getStatusCode());

	}

	public void delete(String url) {
		HttpEntity<String> requestEntity = new HttpEntity<String>("", headers);
		ResponseEntity<String> responseEntity = rest.exchange(url, HttpMethod.DELETE, requestEntity, String.class);
		this.setStatus(responseEntity.getStatusCode());
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}
}
