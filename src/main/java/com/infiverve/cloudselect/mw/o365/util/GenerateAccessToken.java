package com.infiverve.cloudselect.mw.o365.util;

import java.io.IOException;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

@Configuration
@PropertySource("classpath:config.properties")
public class GenerateAccessToken {

	public static String getAccessToken(String grantType, String resource, String clientId, String clientSecret,
			String loginHostname, String resellerDomainPrefix,String username,String password) {
		String responseString = "";
		final String hostname = loginHostname;
		final String url = hostname + "/" + resellerDomainPrefix + "/oauth2/token?api-version=1.0%20HTTP%2F1.1";
		Response response;

		System.out.println("grantType"+grantType);
		System.out.println("resource"+resource);
		System.out.println("clientId"+clientId);
		System.out.println("clientSecret"+clientSecret);
		System.out.println("loginHostname"+loginHostname);
		System.out.println("resellerDomainPrefix"+resellerDomainPrefix);
		System.out.println("username"+username);
		System.out.println("password"+password);
		
		
		
		
		MultipartBuilder builder = new MultipartBuilder();
		builder.addFormDataPart("grant_type", grantType);
		builder.addFormDataPart("resource", resource);
		builder.addFormDataPart("client_id", clientId);
		builder.addFormDataPart("client_secret", clientSecret);
		builder.addFormDataPart("username", username);
		builder.addFormDataPart("password", password);
		final RequestBody body = builder.type(MultipartBuilder.FORM).build();
		final Request request = new Request.Builder().url(url).post(body)
				.addHeader("content-type", "multipart/form-data").addHeader("cache-control", "no-cache").build();
		try {
			response = HTTPClientHelper.getOkHttpclient().newCall(request).execute();
			responseString = response.body().string();
		} catch (IOException e) {
 			e.printStackTrace();
		}
		System.out.println(responseString);
		return new JSONObject(responseString).getString("access_token");
	}
}