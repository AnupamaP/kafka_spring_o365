	package com.infiverve.cloudselect.mw.o365;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages={"com.infiverve.cloudselect.mw.o365"})// same as @Configuration @EnableAutoConfiguration @ComponentScan combined
@EnableAutoConfiguration
public class O365ApiApp {

	public static void main(String[] args) {
		SpringApplication.run(O365ApiApp.class, args);
	}
}
